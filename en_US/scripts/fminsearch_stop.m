% Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
% Copyright (C) 2010 - Digiteo - Michael Baudin
%
% This file must be used under the terms of the CeCILL.
% This source file is licensed as described in the file COPYING, which
% you should have received as part of this distribution.  The terms
% are also available at
% http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

% Test stop criteria

% Basic use
format long
banana = @(x)100*(x(2)-x(1)^2)^2+(1-x(1))^2;
opt = optimset('fminsearch');
opt = optimset(opt,'TolX',0);
opt = optimset(opt,'TolFun',0);
opt = optimset(opt,'MaxIter',inf);
opt = optimset(opt,'MaxFunEvals',inf);
opt = optimset(opt,'OutputFcn', @outfunstop);
[x,fval,exitflag,output] = fminsearch(banana,[-1.2, 1],opt)


